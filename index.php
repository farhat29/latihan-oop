<?php
    require_once('animal.php');
    require_once('frog.php');   
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "name : ".$sheep->animeals."<br>";
    echo "legs : ".$sheep->legs."<br>";
    echo "cold blooded : ".$sheep->cold_blooded."<br><br>";

    $kodok = new Frog("buduk");
    echo "name : ".$kodok->animeals."<br>";
    echo "legs : ".$kodok->legs."<br>";
    echo "cold blooded : ".$kodok->cold_blooded."<br>";
    echo $kodok->jump("hop hop");
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "name : ".$sungokong->animeals."<br>";
    echo "legs : ".$sungokong->legs."<br>";
    echo "cold blooded : ".$sungokong->cold_blooded."<br>";
    echo $sungokong->yell("Auooo");

?>